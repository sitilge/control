# Control Theory

My homework in Control Theory at Riga Technical University written using LyX, gnuplot and MATLAB.

## Notes

- Make sure to set Document->Settings->Language->Language to Latvian and Document->Settings->Language->Lanuage package to None. 
- Replace the Document->Settings->LaTeX Preamble with the content of `src/preamble.txt`.
